//
//  ContentView.swift
//  landmarks
//
//  Created by Samy CHBINOU ESNA on 26/02/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                
            Text("Hello, Samy!")
                .font(.title)
                
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
