//
//  landmarksApp.swift
//  landmarks
//
//  Created by Samy CHBINOU ESNA on 26/02/2023.
//

import SwiftUI

@main
struct landmarksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
